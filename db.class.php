<?php 
/******************************************************
 * File Name	: db.class.php
 *******************************************************/	
	include("includes/config.php");

	#FUNCTION TO CLEAR INPUT VARIABLES 
	function cleanVar($input)  {
	   //$input = mysql_real_escape_string($input);
		if(!is_array($input))
		{
		$input = htmlspecialchars($input, ENT_IGNORE, 'utf-8');
	   	$input = strip_tags($input);
	   	$input = stripslashes($input);
	   	$input = preg_replace("/<script.*?\/script>/s", "", $input);	
		}
	   return $input;
	}
	function removeSpecial($string){
		$newString = preg_replace("/[^a-zA-Z]/", "", $string);
		return $newString;
	}
#class DB extends PDO Class
class DB extends PDO
{
	private $dbname   	= 	WEBSITE_DB_DATABASE;
	private $hostname 	= 	DB_HOST;
	private $username 	=	DB_USERNAME;
    private $password 	=	DB_PASSWORD;
	private $connection = 	NULL;
	
    #make a connection
    public function __construct($DB = '') {
    	if($DB)
			$this->dbname = $DB;
		$this->engine = 'mysql';
        $dns = $this->engine.':dbname='.$this->dbname.";host=".$this->hostname; 
		parent::__construct( $dns, $this->username, $this->password );
        try 
        { 
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        }
        catch (PDOException $e) 
        {
            die($e->getMessage());
        }
    }
	
	
	#select Functions
	
	/* add_order Function */
	public function add_order($productnm, $distributornm, $salespersonnm, $quantity, $shopnm,$order_date,$order_by,$distributorid,$superstockistid,$superstockistnm,$categorynm,$catid,$brandid,$brandnm,$salespfullnm,$oplacelat,$oplacelon,$shop_id) {
		 
		$id=$order_by;
		
		if($distributorid=="")
			$distributorid=0;
		
		/*$sthsstockist = $this->prepare("SELECT external_id FROM tbl_user WHERE id = :id");
		$sthsstockist->bindParam('id', $id);
		$sthsstockist->execute();
		$resultss = $sthsstockist->fetch(PDO::FETCH_ASSOC);
		$distributorid=$resultss['external_id'];*/

		//distributornm name
		/*$sthssname = $this->prepare("SELECT firstname FROM tbl_user WHERE id=".$distributorid."");
		$sthssname->execute();
		$resultsname = $sthssname->fetch(PDO::FETCH_ASSOC);
		$distributornm=$resultsname['firstname'];
		*/

		$sths = $this->prepare("SELECT external_id FROM tbl_user WHERE id=".$distributorid."");
		$sths->execute();
		$results = $sths->fetch(PDO::FETCH_ASSOC);
		$superstockistid=$results['external_id'];

		$sthsname = $this->prepare("SELECT firstname FROM tbl_user WHERE id=".$superstockistid."");
		$sthsname->execute();
		$resultsnm = $sthsname->fetch(PDO::FETCH_ASSOC);
		$superstockistnm=$resultsnm['firstname'];

		$sth = $this->prepare("INSERT INTO `tbl_order_app` 
			( `productnm`, `distributornm`, `salespersonnm`, `quantity`, `shopnm`,order_date,`order_by`, `distributorid`, `superstockistid`,superstockistnm,categorynm,catid,brandid,brandnm,salespfullnm,oplacelat,oplacelon,shop_id)
			 VALUES ( :productnm, :distributornm, :salespersonnm, :quantity, :shopnm, :order_date,:order_by, :distributorid, :superstockistid,:superstockistnm,:categorynm,:catid,:brandid,:brandnm,:salespfullnm,:oplacelat,:oplacelon,:shop_id)");
			 
		$sth->bindParam('productnm', $productnm);
		$sth->bindParam('distributornm', $distributornm);
		$sth->bindParam('salespersonnm', $salespersonnm);
		$sth->bindParam('quantity', $quantity);
		$sth->bindParam('shopnm', $shopnm);
		$sth->bindParam('order_date', $order_date);
		$sth->bindParam('order_by', $order_by);
		if($distributorid=="")
			$distributorid=0;
		$sth->bindParam('distributorid', $distributorid);
		if($superstockistid=="")
			$superstockistid=0;
		$sth->bindParam('superstockistid', $superstockistid);
		$sth->bindParam('superstockistnm', $superstockistnm);
		$sth->bindParam('categorynm', $categorynm);
		$sth->bindParam('catid', $catid);		
		$sth->bindParam('brandid', $brandid);
		$sth->bindParam('brandnm', $brandnm);
		$sth->bindParam('salespfullnm', $salespfullnm);
		$sth->bindParam('oplacelat', $oplacelat);
		$sth->bindParam('oplacelon', $oplacelon);
		$sth->bindParam('shop_id', $shop_id);
		$sth->execute();
		return $this->lastInsertId();
	}
	public function add_varient($orderappid, $variantweight, $weightquantity, $unit, $variantsize, $variantunit, $shopid, $shopnme, $orderid, $totalcost,$product_varient_id, $productid, $productnm, $campaign_applied=null, $campaign_type=null, $campaign_sale_type=null) {
		$fields = "";
		$values = "";
		
		if($product_varient_id=="") {
			$product_varient_id=0;
		}
		
		
		if($campaign_applied == 'yes')
		{			
			$fields.= ", campaign_type, campaign_sale_type";
			$values.= ", :campaign_type, :campaign_sale_type";
			
		}
		
		$sth = $this->prepare("INSERT INTO `tbl_variant_order` 
			( `orderappid`, `variantweight`, weightquantity, unit, `variantsize`, `variantunit`, `shopid`, `shopnme`, `orderid`, `totalcost`, `productid`, product_varient_id, `productnm`,`campaign_applied` $fields) 
			VALUES (:orderappid, :variantweight, :weightquantity, :unit , :variantsize, :variantunit, :shopid, :shopnme, :orderid, :totalcost, :productid, :product_varient_id, :productnm, :campaign_applied $values)");
		$sth->bindParam('orderappid', $orderappid);
		$sth->bindParam('variantweight', $variantweight);
		
		$sth->bindParam('weightquantity', $weightquantity);
		$sth->bindParam('unit', $unit);
		
		$sth->bindParam('variantsize', $variantsize);
		$sth->bindParam('variantunit', $variantunit);
		$sth->bindParam('shopid', $shopid);
		$sth->bindParam('shopnme', $shopnme);
		$sth->bindParam('orderid', $orderid);
		$sth->bindParam('totalcost', $totalcost);
		$sth->bindParam('productid', $productid);
        $sth->bindParam('productnm', $productnm);
		$sth->bindParam('product_varient_id', $product_varient_id);
				
		if($campaign_applied == 'yes')
		{
			$campaign_applied = '0';
			$sth->bindParam('campaign_applied', $campaign_applied);
			$sth->bindParam('campaign_type', $campaign_type);
			$sth->bindParam('campaign_sale_type', $campaign_sale_type);
		}
		else{
			$campaign_applied = '1';
			$sth->bindParam('campaign_applied', $campaign_applied);
		}
		
		$sth->execute();
		
		return $this->lastInsertId();
	}
	public function update_order($orderappid, $quantity, $offer_provided) {

		$sth = $this->prepare("UPDATE `tbl_order_app` SET `quantity` = :quantity, `offer_provided` = :offer_provided WHERE `tbl_order_app`.`id` = :orderappid");
		$sth->bindParam('orderappid', $orderappid);
		$sth->bindParam('quantity', $quantity);
		$sth->bindParam('offer_provided', $offer_provided);
		$sth->execute();
		return $this->lastInsertId();
	}
	function fncheckssstockist($id){
		$sthsstockist = $this->prepare("SELECT external_id FROM tbl_user WHERE id = :id");
		$sthsstockist->bindParam('id', $id);
		$sthsstockist->execute();
		$resultss = $sthsstockist->fetch(PDO::FETCH_ASSOC);
		$superstockistid=$resultss['external_id'];

		//superstockist name
		$sthssname = $this->prepare("SELECT firstname FROM tbl_user WHERE id=".$resultss['external_id']."");
		$sthssname->execute();
		$resultsname = $sthssname->fetch(PDO::FETCH_ASSOC);
		$superstockistnm=$resultsname['firstname'];

		$sths = $this->prepare("SELECT external_id FROM tbl_user WHERE id=".$resultss['external_id']."");
		$sths->execute();
		$results = $sths->fetch(PDO::FETCH_ASSOC);
		$distributorid=$results['external_id'];

		$sthsname = $this->prepare("SELECT firstname FROM tbl_user WHERE id=".$results['external_id']."");
		$sthsname->execute();
		$resultsnm = $sthsname->fetch(PDO::FETCH_ASSOC);
		$distributornm=$resultsnm['firstname'];

		echo $superstockistid."--".$superstockistnm."--".$distributorid."--".$distributornm;
		exit;
	}
	function add_order_c_p_discount($campaign_id,$order_variant_id,$discount_amount,$discount_percent,$actual_amount)
	{
		$sth = $this->prepare("INSERT INTO `tbl_order_cp_discount` 
			( `campaign_id`, `order_variant_id`,  `discount_amount` ,  `discount_percent` , `actual_amount`) 
			VALUES (:campaign_id, :order_variant_id, :discount_amount, :discount_percent , :actual_amount)");
		$sth->bindParam('campaign_id', $campaign_id);
		$sth->bindParam('order_variant_id', $order_variant_id);		
		$sth->bindParam('discount_amount', $discount_amount);
		$sth->bindParam('discount_percent', $discount_percent);		
		$sth->bindParam('actual_amount', $actual_amount);
		
		$sth->execute();
		return $this->lastInsertId();
	}
	function add_order_c_n_f_product($campaign_id,$c_p_order_variant_id,$f_p_order_variant_id)
	{
		$sth = $this->prepare("INSERT INTO `tbl_order_c_n_f_product` 
			( `campaign_id`, `c_p_order_variant_id`,  `f_p_order_variant_id`) 
			VALUES (:campaign_id, :c_p_order_variant_id, :f_p_order_variant_id)");
		$sth->bindParam('campaign_id', $campaign_id);
		$sth->bindParam('c_p_order_variant_id', $c_p_order_variant_id);		
		$sth->bindParam('f_p_order_variant_id', $f_p_order_variant_id);
				
		$sth->execute();
		return $this->lastInsertId();
	}
	function add_order_purchase_p_weight($campaign_id,$order_variant_id,$total_wt_purchase,$total_wt_purchase_unit)
	{
		$sth = $this->prepare("INSERT INTO `tbl_order_purchase_p_weight` 
			( `campaign_id`, `order_variant_id`,  `total_wt_purchase`,`total_wt_purchase_unit`) 
			VALUES (:campaign_id, :order_variant_id, :total_wt_purchase, :total_wt_purchase_unit)");
		$sth->bindParam('campaign_id', $campaign_id);
		$sth->bindParam('order_variant_id', $order_variant_id);		
		$sth->bindParam('total_wt_purchase', $total_wt_purchase);
		$sth->bindParam('total_wt_purchase_unit', $total_wt_purchase_unit);
				
		$sth->execute();
		return $this->lastInsertId();
	}
}
?>