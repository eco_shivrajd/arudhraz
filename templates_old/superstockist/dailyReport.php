<?php
include ("../connection/connection.php");
extract($_POST);
$sql = "SELECT
	shops.name as shopname, shops.address, salesman.firstname,salesman.mobile, OA.order_date , OA.categorynm,  VO.orderid, VO.variantweight  
FROM 
	tbl_order_app OA LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
LEFT JOIN
	tbl_shops shops ON shops.id= VO.shopid
LEFT JOIN
	tbl_user salesman ON salesman.id= OA.order_by
	
WHERE 
	date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' ";

$condition = "";

if($dropdownSalesPerson  !="")
{
	$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
}
if($dropdownSalesPerson=="") {
if($dropdownStockist  !="")
{
	$condition .= " AND OA.distributorid = " . $dropdownStockist;
}
}
if($dropdownStockist  =="") {
if($cmbSuperStockist !="")
{
	$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
}
}
if($dropdownbrands  !="")
{
	$condition .= " AND OA.brandid = " . $dropdownbrands;
} 
if($dropdownCategory  !="")
{
	$condition .= " AND OA.catid = " . $dropdownCategory;
}
if($dropdownshops  !="")
{
	$condition .= " AND VO.shopid = " . $dropdownshops;
}
if($dropdownProducts  !="")
{
	$condition .= " AND VO.productid = " . $dropdownProducts;
}
if($dropdownCity !="")
{
	$condition .= " AND shops.city = " . $dropdownCity;
}
if($dropdownState !="")
{
	$condition .= " AND shops.state = " . $dropdownState;
}
$sql .= $condition;
$result1 = mysqli_query($con,$sql); ?>

<div class="portlet box blue-steel">
	<div class="portlet-title">
		<div class="caption"><i class="icon-puzzle"></i>Daily Sales Report</div>
	</div>
	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Date & Time</th>
						<th>Parties name & address</th>
						<th>Contact Person & Tel. No.</th>
						<th>Remark / Notes / Orders</th>
					</tr>
				</thead>
				<? while($row = mysqli_fetch_array($result1)) { 
					$shopnm = $row["shopname"];
					if($row["address"]!="")
						$shopnm .= "<br>" . $row["address"];				
					$ContactPerson = $row["firstname"];
					
					if($row["mobile"]!="")
						$ContactPerson .= "<br>" . $row["mobile"];
				?>
				<tbody>
					<tr>
						<td><?=$row["order_date"];?></td>
						<td><?=$shopnm;?></td>
						<td><?=$ContactPerson;?></td>
						<td><?=$row["categorynm"] . " - " . $row["variantweight"] . " - ".  $row["orderid"];?></td>
					</tr>
				</tbody>
				<? } ?>
			</table>
		</div>
	</div>
</div>
 