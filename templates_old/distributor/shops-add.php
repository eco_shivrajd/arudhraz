<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/distributor_sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shops
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage Supply Chain</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="shops.php">Shops</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Shop</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Shops
							</div>
							
						</div>
						
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
<?php
if(isset($_POST['submit']))
{
$name=$_POST['name'];
$address=$_POST['address'];
$state=$_POST['state'];
$city =$_POST['city'];
$mobile =$_POST['mobile'];
$contact_person= $_POST['contact_person'];
$shop_added_by=$_SESSION['user_id'];
$sql1 = mysqli_query($con,"INSERT INTO tbl_shops (`name`,`address`,`city`,`state`,`contact_person`,`mobile`,`shop_added_by`) 
VALUES('".$name."','".$address."','".$city."','".$state."','".$contact_person."','".$mobile."','".$shop_added_by."')");
 echo '<script>alert("Shop added successfully.");location.href="shops.php";</script>';
}	
?>                        
                          
           <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="shops-add.php">         
            <div class="form-group">
              <label class="col-md-3">Shop Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="name"
				placeholder="Enter Shop Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter shop name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9- ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address"  
				rows="4"
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address."
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9,./() ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control"></textarea>
              </div>
            </div><!-- /.form-group -->

            <div class="form-group">
              <label class="col-md-3">State:<span class="mandatory">*</span></label>

              <div class="col-md-4">
              <select name="state" class="form-control" 
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select state."
              onChange="showUser(this.value)">
              <option selected disabled>-select-</option>
<?php
$sql="SELECT * FROM tbl_state where country_id=101";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
echo "<option value='$cat_id'>" . $row['name'] . "</option>";
}
?>
</select>
              </div>
            </div><!-- /.form-group -->
     <div id="Subcategory"></div>       
            
            <div class="form-group">
              <label class="col-md-3">Contact Person:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="contact_person"
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter contact person name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            
            <div class="form-group">
              <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="mobile" 
				placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number."
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed."
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
               <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="shops.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>  
                            
                            
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<script>  
function showUser(str)
{
if (str=="")
{
document.getElementById("Subcategory").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch.php?cat_id="+str,true);
xmlhttp.send();
}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>