<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
$id=$_GET['id'];
$name=$_POST['name'];
$address=$_POST['address'];
$state=$_POST['state'];
$city=$_POST['city'];
$contact_person=$_POST['contact_person'];
$mobile=$_POST['mobile'];
$update_sql=mysqli_query($con,"UPDATE tbl_shops SET name='$name',address='$address',state='$state',city='$city',contact_person='$contact_person',mobile='$mobile' where id='$id'");
header('location:shops.php');
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/distributor_sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shops
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage Supply Chain</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="shops.php">Shops</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">View Shop</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								View Shop
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                        
  <?php
$id=$_GET['id'];
$sql1="SELECT * FROM tbl_shops where id = '$id' ";
$result1 = mysqli_query($con,$sql1);

if(mysqli_num_rows($result1)>0)
{
	$row1 = mysqli_fetch_array($result1);
?>                         
  <form class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">       
            <div class="form-group">
              <label class="col-md-3">Shop Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="name" 
				placeholder="Enter Shop Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter shop name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9- ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control" value="<?php echo $row1['name']?>" readonly>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address" rows="4" 
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address."
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9,./() ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control" readonly><?php echo $row1['address']?></textarea>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">State:</label>

              <div class="col-md-4">
<select name="state" 
data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select state."
class="form-control" readonly><!-- onChange="showUser(this.value)" -->
<?php
$sql="SELECT * FROM tbl_state where id='".$row1['state']."'";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);
$cat_id=$row['id'];
echo "<option value='$cat_id'>" . $row['name'] . "</option>";
?>
</select>
              </div>
            </div><!-- /.form-group -->
			
            <div class="form-group">
              <label class="col-md-3">City:</label>

              <div class="col-md-4">
<select name="city" id="city"
data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select city."
class="form-control" readonly>
<?php
$state= $row1['state'];
$sql="SELECT * FROM tbl_city where id='".$row1['city']."'";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);
$cat_id=$row['id'];
echo "<option value='$cat_id'>" . $row['name'] . "</option>";
?>
</select>
              </div>
            </div><!-- /.form-group --> 
        
           <div class="form-group">
              <label class="col-md-3">Suburb:<span class="mandatory">*</span></label>

              <div class="col-md-4">
              <select name="suburbnm" class="form-control" readonly disabled
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select suburb.">
              <option selected disabled>-select-</option>
<?php
$sql="SELECT * FROM tbl_surb";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['suburbid'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['suburbnm'] . "</option>";
}
?>
                </select>
              </div>
            </div><!-- /.form-group -->        
            <div class="form-group">
              <label class="col-md-3">Contact Person:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="contact_person" 
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter contact person name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				class="form-control" value="<?php echo $row1['contact_person']?>" readonly>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">Mobile Number 1:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="mobile" 
				placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number."
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed."
				class="form-control" value="<?php echo $row1['mobile']?>" readonly>
              </div>
            </div><!-- /.form-group -->

            <div class="form-group">
              <label class="col-md-3">Contact Person 2:</label>
              <div class="col-md-4">
                <input type="text" readonly
				placeholder="Enter Contact Person Name"
                data-parsley-trigger="change"				
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				name="contact_person_other"class="form-control" value="<?php echo $row1['contact_person_other']?>">
              </div>
            </div><!-- /.form-group -->          
            
             <div class="form-group">
              <label class="col-md-3">Mobile Number 2:</label>
              <div class="col-md-4">
                <input type="text" name="mobile_number_other"
                placeholder="Enter Mobile Number" readonly
                data-parsley-trigger="change"				
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed."
				class="form-control" value="<?php echo $row1['mobile_number_other'] ?>">
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">GST Shop Number:</label>
              <div class="col-md-4">
                <input type="text" name="gst_number" readonly
                placeholder="Enter GST Shop Number"
                data-parsley-trigger="change"				
				data-parsley-maxlength="20"
				data-parsley-maxlength-message="Only 20 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed."
				class="form-control" value="<?php echo $row1['gst_number']?>">
              </div>
               </div>
			   
            <div class="form-group">
              <label class="col-md-3">Shop closed on day:</label>
              <div class="col-md-4">
			  <?php $day = $row1['closedday']?>
			  <select class="form-control" name="closedday" disabled>
			  <option selected disabled>-Select-</option>
			  <option value="1" <?php if($day == 1){echo "selected";}?>>Monday</option>
			  <option value="2" <?php if($day == 2){echo "selected";}?>>Tuesday</option>
			  <option value="3" <?php if($day == 3){echo "selected";}?>>Wednesday</option>
			  <option value="4" <?php if($day == 4){echo "selected";}?>>Thrusday</option>
			  <option value="5" <?php if($day == 5){echo "selected";}?>>Friday</option>
			  <option value="6" <?php if($day == 6){echo "selected";}?>>Saturday</option>
			  <option value="7" <?php if($day == 7){echo "selected";}?>>Sunday</option>
			  </select>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">Shop open time:</label>
              <div class="col-md-4">
                <div class="input-group date" >
                    <input type="text" class="form-control" name="opentime" id="datetimepicker1" readonly value="<?php echo $row1['opentime']?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
              </div>
            </div><!-- /.form-group -->			     
            <div class="form-group">
              <label class="col-md-3">Shop closed time:</label>
              <div class="col-md-4">
                <div class='input-group date'>
                    <input type="text" class="form-control" name="closetime" id="datetimepicker2" readonly value="<?php echo $row1['closetime']?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                </div>
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Latitude:</label>
              <div class="col-md-4">
                <input type="text" name="latitude" value="<?php echo $row1['latitude']?>"
                placeholder="Enter Latitude" readonly
				class="form-control">
              </div>
            </div><!-- /.form-group -->			
            <div class="form-group">
              <label class="col-md-3">Longitude:</label>
              <div class="col-md-4">
                <input type="text" name="longitude" value="<?php echo $row1['longitude']?>"
                placeholder="Enter Longitude" readonly
				class="form-control">
              </div>
            </div><!-- /.form-group -->		
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
               <!-- <button name="submit" id="submit" class="btn btn-primary">Submit</button> -->
                <a href="shops.php" class="btn btn-primary">Cancel</a>
                <!--<a data-toggle="modal" href="#thankyouModal"  class="btn btn-primary">Delete</a>-->
              </div>
            </div><!-- /.form-group -->

          </form>  

	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
				<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
				</p>                     
        	  <center><a href="shop_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
			  </center>
            </div>    
        </div>
    </div>
</div>		  
                            
<?php } ?>                           
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
<script>  
function showUser(str)
{
if (str=="")
{
document.getElementById("city").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("city").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch_edit.php?cat_id="+str,true);
xmlhttp.send();
}
</script> 