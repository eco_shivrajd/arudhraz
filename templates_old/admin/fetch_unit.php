<?php
include ("../connection/connection.php");
$sql="SELECT * FROM tbl_units";
$result = mysqli_query($con,$sql);
echo "<div class='form-group'>";
echo "<label class='col-md-3'>Unit:<span class='mandatory'>*</span></label>";
echo "<div class='col-md-4'>";
echo "<select name='unitname[]' class='form-control'
             data-parsley-trigger='change'				
              data-parsley-required='#true' 
              data-parsley-required-message='Please select unit.'>
			  <option disabled selected>-Select-</option>";
while($row = mysqli_fetch_array($result))
{
echo "<option value=". $row['id'] .">" . $row['unitname'] . "</option>";
}
echo "</select>";
echo "</div>
            </div>";
mysqli_close($con);
?>